//
//  ClientCommand.swift
//  TTTClient
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import SwiftyJSON

enum ClientMessage {
  case createGame
  case getOpenGames
  case joinGame(gameID: String)
  case makeMove(gameID: String, xcoord: Int, ycoord: Int)
  case error
  
  func toData() -> Data {
    var result: JSON = [:]
    switch self {
    case .createGame:
      result = ["msgType": "createGame"]
    case .getOpenGames:
      result = ["msgType": "getOpenGames"]
    case .joinGame(gameID: let id):
      result = [
        "msgType": "joinGame",
        "params": [
          "gameID": id
        ]
      ]
    case .makeMove(gameID: let id, xcoord: let x, ycoord: let y):
      result = [
        "msgType": "makeMove",
        "params": [
          "gameID": id,
          "xcoord": x,
          "ycoord": y
        ]
      ]
    default:
      result = [:]
    }
    return try! result.rawData()
  }
}

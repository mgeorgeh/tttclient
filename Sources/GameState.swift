//
//  GameState.swift
//  TTTClient
//
//  Created by Charlemagne on 6/19/17.
//
//

import Foundation

class GameState {
  var cells: [[String]] = [
    ["-", "-", "-"],
    ["-", "-", "-"],
    ["-", "-", "-"]
  ]
  
  var gameDisplayStr: String {
    var cells1: [String] = ["", "", ""]
    for (i, row) in cells.map({ $0.joined(separator: "|") }).enumerated() {
      cells1[i] = "\(2 - i) " + row
    }
    
    let instructions = "Enter the space-separated coordinates of the move you would like to make. e.g. 0 0 or 1 2\n"
    
    return instructions + cells1.joined(separator: "\n") + "\n  0 1 2\n" + (isPlayerTurn ? "Your move: " : "waiting for opponent to move")
  }
  
  var isPlayerTurn: Bool
  var playerToken: String
  var gameID: String
  
  init(gameID: String, isPlayerTurn: Bool, playerToken: String) {
    self.gameID = gameID
    self.isPlayerTurn = isPlayerTurn
    self.playerToken = playerToken
  }
  
  func display() {
    Terminal.clear()
    print(gameDisplayStr)
  }
  
  func displayEndCondition(state: GameEndState) {
    Terminal.clear()
    switch state {
    case .tie:
      print("YOU TIE")
    case .win(token: let t):
      if t == playerToken {
        print("YOU WIN")
      } else {
        print("YOU LOSE")
      }
    }
  }
  
  func addToken(_ tokenType: String, _ x: Int, _ y: Int) {
    cells[2 - y][x] = tokenType
  }
  
  func flipPlayerTurn() {
    isPlayerTurn = !isPlayerTurn
  }
}

//
//  ServerResponse.swift
//  TTTClient
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import SwiftyJSON

enum GameEndState {
  case win(token: String)
  case tie
}

enum ServerMessage {
  case gameCreateSuccess
  case gameStart(gameID: String, goingFirst: Bool)
  case listOpenGames(games: [String])
  case moveMade(tokenType: String, xcoord: Int, ycoord: Int)
  case gameEnd(state: GameEndState)
  case error
  
  init(data: Data) {
    let jsonData = JSON(data)
    
    guard let msgType = jsonData["msgType"].string else {
      self = .error
      return
    }
    
    switch msgType {
    case "gameCreateSuccess":
      self = .gameCreateSuccess

    case "gameStart":
      guard let id = jsonData["params"]["gameID"].string,
        let goingFirst = jsonData["params"]["goingFirst"].bool else {
          self = .error
          return
      }
      self = .gameStart(gameID: id, goingFirst: goingFirst)
      
    case "listOpenGames":
      let games = jsonData["params"]["games"].arrayValue.map { $0.stringValue } //error handling?
      self = .listOpenGames(games: games)
      
    case "moveMade":
      guard let tokenType = jsonData["params"]["tokenType"].string,
        let xcoord = jsonData["params"]["xcoord"].int,
        let ycoord = jsonData["params"]["ycoord"].int else {
          self = .error
          return
      }
      
      self = .moveMade(tokenType: tokenType, xcoord: xcoord, ycoord: ycoord)
      
    case "gameEnd":
      guard let tokenStr = jsonData["params"]["tokenStr"].string else {
        self = .error
        return
      }
      
      switch tokenStr {
      case "":
        self = .gameEnd(state: .tie)
      case "x":
        self = .gameEnd(state: .win(token: "x"))
      case "o":
        self = .gameEnd(state: .win(token: "o"))
      default:
        self = .error
      }
      
    default:
      self = .error
    }
  }
}

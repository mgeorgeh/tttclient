//
//  Terminal.swift
//  TTTClient
//
//  Created by Charlemagne on 6/19/17.
//
//

import Foundation

struct Terminal {
  static func clear() {
    print("\u{001B}[2J")
  }
}

//
//  main.swift
//  TTTClient
//
//  Created by Charlemagne on 6/16/17.
//
//

import Foundation
import Starscream

class SockHandler: WebSocketDelegate {
  
  var socket: WebSocket
  var games: [String] = []
  var gameState: GameState?
  var devUrlStr: String = "ws://localhost:8080/tictactoe"
  var prodUrlStr: String = "ws://45.79.191.166:8080/tictactoe"
  
  init() {
    socket = WebSocket(url: URL(string: prodUrlStr)!)
    socket.delegate = self
    socket.callbackQueue = DispatchQueue(label: "com.blah.blah")
    socket.connect()
  }
  
  public func websocketDidConnect(socket: WebSocket) {
//    print("websocket is connected")
  }
  
  public func websocketDidDisconnect(socket: WebSocket, error: NSError?) {
    print("websocket is disconnected: \(error?.localizedDescription ?? "")")
  }
  
  public func websocketDidReceiveMessage(socket: WebSocket, text: String) {
//    print("got some text: \(text)")
  }
  
  public func websocketDidReceiveData(socket: WebSocket, data: Data) {
    let serverMessage = ServerMessage(data: data)
    
    switch serverMessage {
    case .listOpenGames(games: let gs):
      games = gs
      print(games)
      
    case .gameStart(gameID: let id, goingFirst: let b):
      gameState = GameState(gameID: id, isPlayerTurn: b, playerToken: b ? "x" : "o")
      gameState!.display()
      
    case .moveMade(tokenType: let token, xcoord: let x, ycoord: let y):
      gameState!.addToken(token, x, y)
      gameState!.flipPlayerTurn()
      gameState!.display()
      
    case .gameEnd(state: let s):
//      displayEndCondition(state: s)
      gameState!.displayEndCondition(state: s)
    default:
      print("got some data: \(data.count), \(serverMessage)")
    }
    
  }
  
  public func sendText(_ text: String) {
    socket.write(string: text)
  }
  
  public func sendData(_ data: Data) {
    socket.write(data: data)
  }
  
  public func disconnect() {
    if socket.isConnected {
      socket.disconnect()
    }
  }
  
  deinit {
    print("I AM GONE NOW")
  }
}

let socketHandler = SockHandler()

func sendMessage(_ msg: ClientMessage) {
  socketHandler.sendData(msg.toData())
}

func getInts(_ str: String) -> [Int]? {
  return str.components(separatedBy: " ").map{ Int($0)! }
}

Terminal.clear()
let instructions = "C to create game, G to get available games, J <INT> to join an existing game. Q to quit"
print(instructions)

game: while true {
  var stringToSend = readLine()!
  switch stringToSend {
  // J 0, J 1, J 2
  case _ where stringToSend.hasPrefix("J"):
    let gameID = socketHandler.games[Int(stringToSend.components(separatedBy: " ")[1])!]
    sendMessage(.joinGame(gameID: gameID))
    case "G":
    sendMessage(.getOpenGames)
  case "C":
    sendMessage(.createGame)
  case _ where getInts(stringToSend) != nil:
    let ints = getInts(stringToSend)
    sendMessage(.makeMove(gameID: socketHandler.gameState!.gameID, xcoord: ints![0], ycoord: ints![1]))
  default:
    break game
  }
}

// c1: create game
// c2: get open games
// c2: join game by index of id

socketHandler.disconnect()


